# LUIS-evaluation

Completar `.env`:

```
LUIS_APP_ID=<application id>
LUIS_SUB_KEY=<subscription key>
LUIS_VERSION=<version, eg. '0.1'>
LUIS_LOCATION=<location, eg. 'westus'>
AZURE_SUBSCRIPTION_ID=<subscription id>
AZURE_RESOURCE_GROUP=<resource group>
AZURE_RESOURCE_NAME=<resource name>
AZURE_RESOURCE_KEY=<resource key>
```

Se descarga el modelo correspondiente a `LUIS_APP_ID` y `LUIS_VERSION`, se
particiona en *k* folds y se hace K-Fold Cross Validation.

Los *k* modelos para hacer evaluación se crean en el mismo ambiente con el
prefijo `TEST ` en el nombre. Bajar el json y cargar en otro ambiente de LUIS
si no se quiere ocupar el mismo (ver TODO). No hay manejo de errores, por lo
que si falla el script hay que borrar modelos de evaluación manualmente.

Con Python 3:

```
python eval.py <k>
```

Si un intent (o clase) no se puede particionar en *k* partes es ignorado. 4-5
es un valor típico, si se tienen muchos utterances (o instancias) por clase 
10 también es popular.

Se (borra si es que existe y) crea el directorio `output`, donde se crean
los archivos `report-<n>.txt` con un reporte de precisión y cobertura para cada
fold, `cm-<n>.csv` con las matrices de confusión y `cm.csv` la suma de ellas.

Es necesario tener un grupo de recursos en azure y una applicación LUIS para
provisionar el modelo y no tener atados de rate limit.

También hay que tener Azure CLI `az` instalado, estar logeado y tener la
subscripción elegida.

## TODO

* promedio de precision/recall.
* argparse.
* Leer data de entrenamiento del .json en vez de LUIS api.
* LUIS source y target.
* finally: borrar LUISes de evaluación.
* Opción para output dir.
* Graficar matriz de confusión.
* Evaluación cumulativa de folds.
* Dispatch?
* QnA.
