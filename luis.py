#!/usr/bin/env python

from copy import deepcopy
from collections import defaultdict
from json import dumps
from os import getenv
from urllib.parse import quote
from subprocess import run
from time import sleep
from dotenv import load_dotenv
import requests


load_dotenv()
APP_ID = getenv('LUIS_APP_ID')
SUBSCRIPTION_KEY = getenv('LUIS_SUB_KEY')
VERSION = getenv('LUIS_VERSION')
LOCATION = getenv('LUIS_LOCATION')
AZURE_SUBSCRIPTION_ID = getenv('AZURE_SUBSCRIPTION_ID')
AZURE_RESOURCE_GROUP = getenv('AZURE_RESOURCE_GROUP')
AZURE_RESOURCE_NAME = getenv('AZURE_RESOURCE_NAME')
AZURE_RESOURCE_KEY = getenv('AZURE_RESOURCE_KEY')


def get_model(location, app_id, version, subscription_key):
    url = 'https://{}.api.cognitive.microsoft.com/luis/api/v2.0/apps/{}/versions/{}/export'.format(
        location, app_id, version)
    headers = {'Ocp-Apim-Subscription-Key': subscription_key}
    response = requests.get(url, headers=headers)
    if response.status_code != 200:
        raise Exception('[ERROR] get_model(): Status Code ' +
                        str(response.status_code))
    return response.json()


def get_intents_utterances(model_json):
    intents = defaultdict(list)
    for utterance in model_json['utterances']:
        intents[utterance['intent']].append(utterance['text'])
    return intents


def _make_intents(folds):
    intents = []
    for fold in folds:
        for _, intent in folds[fold]:
            if intent not in intents:
                intents.append(intent)
    return [{"name": i} for i in intents]


def _make_utterances(folds):
    utterances = []
    for fold in folds:
        for utterance, intent in folds[fold]:
            utterances.append({'text': utterance, 'intent': intent,
                               'entities': []})
    return utterances


def make_training_json(folds, model_json):
    folds_model = deepcopy(model_json)
    folds_model['name'] = 'TEST ' + folds_model['name']
    folds_model['intents'] = _make_intents(folds)
    folds_model['utterances'] = _make_utterances(folds)
    return folds_model


def train_model(location, subscription_key, model_json):
    url = 'https://{}.api.cognitive.microsoft.com/luis/api/v2.0/apps/import'.format(location)
    headers = {'Content-Type': 'application/json',
               'Ocp-Apim-Subscription-Key': subscription_key}
    response = requests.post(url, headers=headers, data=dumps(model_json))
    if response.status_code != 201:
        raise Exception('[ERROR] train_model()/import: Status Code ' +
                        str(response.status_code))
    app_id = response.json()
    version = '0.1'
    url = 'https://{}.api.cognitive.microsoft.com/luis/api/v2.0/apps/{}/versions/{}/train'.format(
        location, app_id, version)
    response = requests.post(url, headers=headers)
    if response.status_code != 202:
        raise Exception('[ERROR] train_model()/train: Status Code ' +
                        str(response.status_code))
    return app_id


def publish_model(location, app_id, subscription_key):
    url = 'https://{}.api.cognitive.microsoft.com/luis/api/v2.0/apps/{}/publish'.format(
        location, app_id)
    headers = {'Ocp-Apim-Subscription-Key': subscription_key}
    response = requests.post(
        url, headers=headers,
        data=dumps({'versionId': '0.1', 'isStaging': False}))
    if response.status_code != 201:
        raise Exception('[ERROR] publish_model(): Status Code ' +
                        str(response.status_code))


def training_status(location, app_id, version, subscription_key):
    url = 'https://{}.api.cognitive.microsoft.com/luis/api/v2.0/apps/{}/versions/{}/train'.format(
        location, app_id, version)
    headers = {'Ocp-Apim-Subscription-Key': subscription_key}
    response = requests.get(url, headers=headers)
    if response.status_code != 200:
        raise Exception('[ERROR] training_status(): Status Code ' +
                        str(response.status_code))
    return all([m['details']['status'] == 'Success' for m in response.json()])


def get_azure_token():
    """
    Needs Azure CLI, must be logged in and have a chosen subscription.
    TODO: add check for 'az' or find another way of getting token.
    """
    az_exec = run('az account get-access-token --query accessToken -o tsv',
        shell=True, capture_output=True, encoding='utf-8')
    if az_exec.returncode != 0:
        raise Exception('[ERROR] Failed getting token through Azure CLI.')
    token = az_exec.stdout.strip()
    return token


def provision_model(location, app_id, subscription_key, azure_subscription_id,
        azure_resource_group, azure_resource_name, azure_token):
    url = 'https://{}.api.cognitive.microsoft.com/luis/api/v2.0/apps/{}/azureaccounts'.format(
        location, app_id)
    auth_token = 'Bearer ' + azure_token
    headers = {'Ocp-Apim-Subscription-Key': subscription_key,
               'Authorization': auth_token,
               'Content-Type': 'application/json'}
    data = {'azureSubscriptionId': azure_subscription_id,
            'resourceGroup': azure_resource_group,
            'accountName': azure_resource_name}
    resp = requests.post(url, headers=headers, json=data)


def delete_model(location, app_id, subscription_key):
    url = 'https://{}.api.cognitive.microsoft.com/luis/api/v2.0/apps/{}'.format(
        location, app_id)
    headers = {'Ocp-Apim-Subscription-Key': subscription_key}
    response = requests.delete(url, headers=headers)
    if response.status_code != 200:
        raise Exception('[ERROR] delete_model(): Status Code ' +
                        str(response.status_code))
    outp = False
    if response.json()['code'] == 'Success':
        outp = True
    return outp


def query_model(location, app_id, subscription_key, query):
    url = (
        'https://{}.api.cognitive.microsoft.com/luis/v2.0/apps/{}?'
        'verbose=true&timezoneOffset=-360&subscription-key={}&q={}').format(
            location, app_id, subscription_key, quote(query))
    response = requests.get(url)
    if response.status_code != 200:
        raise Exception('[ERROR] query_model(): Status Code ' +
                        str(response.status_code))
    return response.json()['topScoringIntent']['intent']
