#!/usr/bin/env python

from collections import defaultdict
from copy import copy
from functools import partial
import os
from random import shuffle
from shutil import rmtree
import sys
from time import sleep
from sklearn.metrics import confusion_matrix, classification_report
import numpy as np
from luis import (
        APP_ID, SUBSCRIPTION_KEY, VERSION, LOCATION, AZURE_SUBSCRIPTION_ID,
        AZURE_RESOURCE_GROUP, AZURE_RESOURCE_NAME, AZURE_RESOURCE_KEY)
from luis import (
        delete_model, get_intents_utterances, get_azure_token, get_model,
        make_training_json, provision_model, publish_model, query_model,
        training_status, train_model)


def shuffle_utterances(intent_utterance_dict):
    for utterances in intent_utterance_dict.values():
        shuffle(utterances)
    return intent_utterance_dict


def make_folds(intent_utterance_dict, k_folds):
    folds = defaultdict(list)
    current_fold = 0
    for intent in intent_utterance_dict.keys():
        if len(intent_utterance_dict[intent]) < k_folds:
            print(
                "\nIntent '{}' no tiene suficientes utterances para ser divido"
                " en {} folds y será ignorado.".format(intent, k_folds))
            continue
        for utterance in intent_utterance_dict[intent]:
            if current_fold == k_folds:
                current_fold = 0
            folds[current_fold].append((utterance, intent))
            current_fold += 1
    return folds


def evaluate_fold(fold, fold_num, predict_func):
    truth = []
    predicted = []
    print('Evaluando fold {}\n'.format(fold_num+1), end='', flush=True)
    for utterance, intent in fold:
        prediction = predict_func(utterance)
        if prediction == intent:
            print('=', end='', flush=True)
        else:
            print('!', end='', flush=True)
        truth.append(intent)
        predicted.append(prediction)
    print()
    return truth, predicted


def main(k_folds):
    if os.path.isdir('output'):
        rmtree('output')
    os.mkdir('output')
    model_json = get_model(LOCATION, APP_ID, VERSION, SUBSCRIPTION_KEY)
    intent_utterance_dict = shuffle_utterances(
        get_intents_utterances(model_json))
    folds = make_folds(intent_utterance_dict, k_folds)
    conf_matrix = None
    for curr_fold_num in folds.keys():
        testing_fold = folds[curr_fold_num]
        training_folds = copy(folds)
        del training_folds[curr_fold_num]
        training_json = make_training_json(training_folds, model_json)
        model_id = train_model(LOCATION, SUBSCRIPTION_KEY, training_json)
        print('\nEntrenando modelo sin fold {}.'.format(curr_fold_num+1),
              end='', flush=True)
        while not training_status(LOCATION, model_id, '0.1', SUBSCRIPTION_KEY):
            print('.', end='', flush=True)
            sleep(.5)
        publish_model(LOCATION, model_id, SUBSCRIPTION_KEY)
        print('\nProvisionando modelo con recurso de Azure.')
        token = get_azure_token()
        provision_model(
                LOCATION, model_id, SUBSCRIPTION_KEY, AZURE_SUBSCRIPTION_ID,
                AZURE_RESOURCE_GROUP, AZURE_RESOURCE_NAME, token)

        prediction_func = partial(
            query_model, LOCATION, model_id, AZURE_RESOURCE_KEY)
        truth, predicted = evaluate_fold(
            testing_fold, curr_fold_num, prediction_func)
        with open('output/report-{}.txt'.format(curr_fold_num), 'w') as f:
            f.write(classification_report(truth, predicted))
        fold_cm = confusion_matrix(
            truth, predicted, labels=sorted(set(intent for utterance,
                                                intent in testing_fold)))
        np.savetxt(
            "output/cm-{}.csv".format(curr_fold_num), fold_cm,
            fmt='%.0f', delimiter=',', header=', '.join(
                sorted(set(intent for utterance, intent in folds[0]))))
        if conf_matrix is None:
            conf_matrix = fold_cm
        else:
            conf_matrix = np.add(conf_matrix, fold_cm)
        delete_model(LOCATION, model_id, SUBSCRIPTION_KEY)
    np.savetxt(
        "output/cm.csv", conf_matrix, fmt='%.0f', delimiter=',',
        header=', '.join(
            sorted(set(intent for utterance, intent in folds[0]))))
    print('\nListo!')


if __name__ == '__main__':
    k = int(sys.argv[1])
    main(k)
